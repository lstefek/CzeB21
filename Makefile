MOD_DIR=modules/texts/ztext/czeb21
MODULES=$(MOD_DIR)/nt.bzz $(MOD_DIR)/ot.bzz
#OSIS2MOD=/home/ls/cmake_sword/utilities/osis2mod
OSIS2MOD=osis2mod

all: zip2crosswire

$(MODULES): CzeB21.xml
	mkdir -p $(MOD_DIR)
	#ldd $(OSIS2MOD)
	$(OSIS2MOD) $(MOD_DIR)  $< -z -v Luther
	#$(OSIS2MOD) $(MOD_DIR)  $< -z -v B21

module: $(MODULES) czeb21.conf
	install -D -m 644 czeb21.conf mods.d/czeb21.conf
	zip -9vT CzeB21.zip $(MOD_DIR)/* mods.d/*

zip2crosswire: $(MODULES) czeb21.conf
	zip -9vT CzeB21.zip CzeB21.xml czeb21.conf

clean:
	rm -f *~ $(MODULES) CzeB21.zip
